package com.example.demo.controller;

import com.example.demo.dto.AuthDTO;
import com.example.demo.service.IAuthService;
import com.example.demo.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：lichuanbin
 * @since : 2021/2/26 18:11
 */
@RestController
@RequestMapping("/user")
public class AuthController {

    @Autowired
    private IAuthService authService;

    /**
     * 实现登录
     * @param authDTO
     * @return
     */
    @PostMapping("/login")
    public ResponseUtils login(HttpServletResponse response, HttpServletRequest request,@RequestBody AuthDTO authDTO){
        return authService.login(request,response,authDTO);
    }

    /**
     * 退出登录
     * @param request
     * @param response
     * @return
     */
    @PostMapping("/logout")
    public ResponseUtils logout(HttpServletRequest request,HttpServletResponse response){
       return authService.logout(request,response);
    }
}
