package com.example.demo.controller;


import com.example.demo.query.student.StudentFileQuery;
import com.example.demo.service.FileService;
import com.example.demo.utils.ResponseUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ：lichuanbin
 * @since : 2021/4/12 17:07
 */
@RestController
@RequestMapping("/file")
public class FileController {
    @Autowired
    private FileService fileService;

    @PostMapping("/upload")
    public ResponseUtils upload(StudentFileQuery fileQuery) throws IOException {
        return fileService.upload(fileQuery);
    }

    @GetMapping("/download")
    public void download(HttpServletResponse response,@RequestParam("fileId") String fileId) throws IOException {
        fileService.download(response,fileId);
    }

    /**
     * 获取文件列表
     * @param studebtNum
     * @return
     */
    @PostMapping("/fileList")
    public ResponseUtils fileList(@RequestParam("studentNum")Long studebtNum){
        return fileService.fileList(studebtNum);
    }
}
