package com.example.demo.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;
import com.example.demo.mapper.TeacherMapper;
import com.example.demo.query.student.*;
import com.example.demo.service.FileService;
import com.example.demo.service.IPaperService;
import com.example.demo.service.IStudentService;
import com.example.demo.service.IStudentWeeklyNoteService;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.h5.H5StudentInfoVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.management.Query;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * @author ：lichuanbin
 * @since : 2021/3/5 11:40
 *
 * 学生权限
 */
@RestController
@RequestMapping(value = "/student", produces = "application/json;charset=UTF-8")
@Slf4j
public class StudentController {
    @Autowired
    private IStudentService studentService;
    @Autowired
    private TeacherMapper teacherMapper;
    /**
     * 学生编辑信息
     * @param editQuery
     * @return
     */
    @PostMapping("/editStudent")
    public ResponseUtils editStudent(@RequestBody StudentEditQuery editQuery){
        return studentService.editStudent(editQuery);
    }

    /**
     * 查询学生本人信息
     * @param studentNum
     * @return
     */
    @PostMapping("/studentInfo")
    public ResponseUtils studentInfo(@RequestParam("studentNum") Integer studentNum){
        ResponseUtils responseUtils=new ResponseUtils();
        H5StudentInfoVo vo=new H5StudentInfoVo();
        QueryWrapper<Student> wrapper=new QueryWrapper<>();
        wrapper.eq("student_num",studentNum);
        BeanUtils.copyProperties(studentService.getOne(wrapper),vo);
        QueryWrapper<Teacher> wrapper1=new QueryWrapper<>();
        Teacher teacher=teacherMapper.selectOne(wrapper1.eq("teacher_num",studentService.getOne(wrapper).getTeacherNum()));
        vo.setTeacherName(teacher.getTeacherName());
        return responseUtils.code(HttpStatus.OK).data(vo).message("成功");
    }

    /**
     * 修改学生密码
     * @param updataPassword
     * @return
     */
    @PostMapping("/studentPasswordUpdata")
    public ResponseUtils studentPasswordUpdata(@RequestBody StudentUpdataPasswordQuery updataPassword){
        return studentService.editStudent(updataPassword);
    }


    /**
     * 论文模块
     */
    private IPaperService paperService;

    /**
     * 显示论文信息
     * @param paperListQuery
     * @return
     */
    @PostMapping("/studentPaperList")
    public ResponseUtils paperList(@RequestBody StudentPaperListQuery paperListQuery){
        return studentService.selectPaperList(paperListQuery);
    }

    /**
     * 学生添加论文
     * @param addPaperQuery
     * @return
     */
    @PostMapping("/studentAddPaper")
    public ResponseUtils studentAddPaper(@RequestBody StudentAddPaperQuery addPaperQuery){
        return studentService.addPaper(addPaperQuery);
    }

    /**
     * 学生选择论文
     * @param paperId
     * @param studentNum
     * @return
     */
    @PostMapping("/studentCheckedPaper")
    public ResponseUtils studentCheckedPaper(@RequestParam("paperId") Long paperId,@RequestParam("studentNum") Long studentNum){
        return studentService.studentCheckedPaper(paperId,studentNum);
    }

    /**
     * 查询学生本人论文
     * @param studentNum
     * @return
     */
    @PostMapping("/selectStudentPaper")
    public ResponseUtils selectStudentPaper(@RequestParam("studentNum") Long studentNum){
        return studentService.selectStudentPaper(studentNum);
    }


    /**
     * 文件上传获取模块
     */

    @Autowired
    private FileService fileService;

    /**
     *
     * @param fileQuery
     * @return
     */
    @PostMapping("/uploadPaper")
    public ResponseUtils uploadPaper(StudentFileQuery fileQuery) throws IOException {
        return fileService.upload(fileQuery);
    }


    /**
     * 周记模块
     */

    @Autowired
    private IStudentWeeklyNoteService noteService;

    /**
     * 学生添加周记
     * @param noteContent
     * @param studentNum
     * @return
     */
     @PostMapping("/addStudentWeeklyNote")
     public ResponseUtils addStudentWeeklyNote(@RequestParam("noteContent") String noteContent,@RequestParam("studentNum") Long studentNum){
         return noteService.addStudentWeeklyNote(noteContent,studentNum);
     }

    /**
     * 显示周记列表
     * @param studentNum
     * @return
     */
     @PostMapping("/showStudentWeeklyNoteList")
     public ResponseUtils showStudentWeeklyNoteList(@RequestParam("studentNum") Long studentNum){
         return noteService.showStudentWeeklyNoteList(studentNum);
     }
}
