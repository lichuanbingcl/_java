package com.example.demo.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.example.demo.entity.Teacher;
import com.example.demo.query.teacher.*;
import com.example.demo.service.*;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.oms.OmsTeacherListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ：lichuanbin
 * @since : 2021/3/15 16:34
 *
 * 教师权限
 */
@RestController
@RequestMapping("/teacher")
public class TeacherController {
    @Autowired
    private IStudentService studentService;

    @Autowired
    private ITeacherService teacherService;

    @Autowired
    private FileService fileService;
    /**
     * 文件下载
     */
    @PostMapping("/download")
    public void download(HttpServletResponse response,String fileId) throws IOException {
        fileService.download(response, fileId);
    }

    /**
     * 学生模块
     */

    /**
     * 添加学生
     * @param addQuery
     * @return
     */
    @PostMapping("/addStudent")
    @ResponseBody
    public ResponseUtils addStudent(@RequestBody StudentAddQuery addQuery){
        return studentService.addStudent(addQuery);
    }

    /**
     * 删除学生
     * @param studentId
     * @return
     */
    @PostMapping("/deleteStudent")
    public ResponseUtils deleteStudent(@RequestParam("studentId") Long studentId){return studentService.deleteStudent(studentId);}

    /**
     * 编辑学生信息
     * @param editQuery
     * @return
     */
    @PostMapping("/editStudent")
    public ResponseUtils editStudent(@RequestBody TeacherEditStudentQuery editQuery){return  studentService.editStudent(editQuery);}

    /**
     * 查询学生信息，按学号正序排序
     * @param selectQuery
     * @return
     */
    @PostMapping("/selectStudentList")
    public ResponseUtils selectStudentList(@RequestBody StudentSelectQuery selectQuery){return studentService.selectStudentList(selectQuery);}


    /**
     * 教师模块
     */

    /**
     * 添加教师
     * @param addQuery
     * @return
     */
    @PostMapping("/addTeacher")
    public ResponseUtils addTeacher(@RequestBody TeacherAddQuery addQuery){
        return teacherService.addTeacher(addQuery);
    }

    /**
     * 编辑教师信息
     *
     * @param editQuery
     * @return
     */
    @PostMapping("editTeacher")
    public ResponseUtils editTeacher(@RequestBody TeacherEditQuery editQuery){
        return teacherService.editTeacher(editQuery);
    }

    /**
     * 删除教师信息
     *
     * @param teacherId
     * @return
     */
    @PostMapping("/deleteTeacher")
    public ResponseUtils deleteTeacher(@RequestParam("teacherId") Long teacherId ){
        ResponseUtils responseUtils=new ResponseUtils();
        if (ObjectUtils.isEmpty(teacherService.getById(teacherId))){
            return responseUtils.code(HttpStatus.OK).data("该账号不存在").message("成功");
        }
        return responseUtils.code(HttpStatus.OK).data(teacherService.removeById(teacherId)).message("成功");
    }

    /**
     * 查询教师列表
     * @param selectQuery
     * @return
     */
    @PostMapping("/selectTeacherList")
    public ResponseUtils selectTeacherList(@RequestBody TeacherSelectQuery selectQuery){
        return teacherService.selectTeacherList(selectQuery);
    }

    /**
     * 教师信息
     * @param teacherNum
     * @return
     */
    @PostMapping("/teacherInfo")
    public ResponseUtils teacherInfo(@RequestParam("teacherNum") Long teacherNum){
        ResponseUtils responseUtils=new ResponseUtils();
        OmsTeacherListVo vo=new OmsTeacherListVo();
        QueryWrapper<Teacher> wrapper=new QueryWrapper<>();
        wrapper.eq("teacher_num",teacherNum);
        BeanUtils.copyProperties(teacherService.getOne(wrapper),vo);
        return responseUtils.code(HttpStatus.OK).data(vo).message("成功");
    }

    /**
     * 修改教师密码
     * @param updateQuery
     * @return
     */
    @PostMapping("/teacherPasswordUpdate")
    public ResponseUtils studentPasswordUpdata(@RequestBody TeacherPasswordUpdateQuery updateQuery){
        return teacherService.editTeacher(updateQuery);
    }


    /**
     * 论文模块
     */

    @Autowired
    private IPaperService paperService;

    /**
     * 添加论文
     * @param addQuery
     * @return
     */
    @PostMapping("/addPaper")
    public ResponseUtils addPaper(@RequestBody PaperAddQuery addQuery){
        return paperService.addPaper(addQuery);
    }

    /**
     * 编辑论文
     * @param editQuery
     * @return
     */
    @PostMapping("/editPaper")
    public ResponseUtils editPaper(@RequestBody PaperEditQuery editQuery){
        return paperService.editPaper(editQuery);
    }

    /**
     * 删除论文
     * @param paperId
     * @return
     */
    @PostMapping("/deletePaper")
    public ResponseUtils deletePaper(@RequestParam("paperId") Long paperId){
        ResponseUtils responseUtils=new ResponseUtils();
        if (ObjectUtils.isEmpty(paperService.getById(paperId))){
            return responseUtils.code(HttpStatus.OK).message("成功").data("该论文不存在");
        }
        return responseUtils.code(HttpStatus.OK).message("成功").data(paperService.removeById(paperId));
    }

    /**
     * 查询论文列表
     * @param selectQuery
     * @return
     */
    @PostMapping("/selectPaperList")
    public ResponseUtils selectPaperList(@RequestBody PaperSelectQuery selectQuery){
        return paperService.selectPaperList(selectQuery);
    }

    /**
     * 显示论文分数
     * @param scoreListQuery
     * @return
     */
    @PostMapping("/showPaperScoreList")
    public ResponseUtils shoePaperScoreList(@RequestBody PaperScoreListQuery scoreListQuery){
        return paperService.showPaperScoreList(scoreListQuery);
    }

    /**
     * 添加论文分数
     * @param paperId
     * @param paperScore
     * @return
     */
    @PostMapping("/addPaperScore")
    public ResponseUtils addPaperScore(@RequestParam("paperId") Long paperId,@RequestParam("paperScore") Integer paperScore){
        return paperService.addPaperScore(paperId,paperScore);
    }

    /**
     * 周记模块
     */

    @Autowired
    private IStudentWeeklyNoteService noteService;

    /**
     * 教师添加周记评价
     * @param noteId
     * @param noteEvaluate
     * @return
     */
    @PostMapping("/editNote")
    public ResponseUtils editNote(@RequestParam("noteId") Long noteId,@RequestParam("noteEvaluate") String noteEvaluate){
        return noteService.editNote(noteId,noteEvaluate);
    }

    /**
     * 显示论文周记
     * @param studentNum
     * @return
     */
    @PostMapping("showStudentWeeklyNoteList")
    public ResponseUtils showStudentWeeklyNoteList(@RequestParam("studentNum") Long studentNum){
        return noteService.showStudentWeeklyNoteList(studentNum);
    }
}
