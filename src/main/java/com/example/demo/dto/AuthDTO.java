package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.HttpRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author ：lichuanbin
 * @since : 2021/2/25 16:39
 *
 * 登录请求信息
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AuthDTO {
    private Long userAccount;
    private String password;
    private String standing;
}
