package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/4/12 15:37
 */
@Data
public class File {
    /**
     * 文件id
     */
    @TableId(type = IdType.INPUT)
    private String fileId;
    /**
     * 文件名
     */
    private String fileName;
    /**
     * 文件正文
     */
    private Object fileContent;
    /**
     * 文件描述
     */
    private String fileDescription;
    /**
     * 学生号
     */
    private Long studentNum;
    /**
     * 教师号
     */
    private Long teacherNum;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer ctime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer utime;
    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer isDelete;
}
