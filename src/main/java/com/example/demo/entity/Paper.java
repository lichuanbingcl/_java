package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：lichuanbin
 * @since : 2021/3/16 17:52
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Paper {
    /**
     * 论文id
     */
    @TableId(type = IdType.AUTO)
    private Long paperId;
    /**
     * 论文标题
     */
    private String paperTitle;
    /**
     * 论文类型
     */
    private String paperType;
    /**
     * 导师id
     */
    private Long teacherNum;
    /**
     * 学生id
     */
    private Long studentNum;
    /**
     * 论文来源
     */
    private String paperSource;
    /**
     * 论文要求
     */
    private String paperRequire;
    /**
     * 论文分数
     */
    private Integer paperScore;
    /**
     * 选择状态
     */
    private String selectState;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer ctime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer utime;
    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer isDelete;
}
