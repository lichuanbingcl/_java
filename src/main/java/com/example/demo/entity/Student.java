package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author ：lichuanbin
 * @since : 2021/2/22 14:02
 *
 * 学生信息表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Student implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 学生id
     */
    @TableId(type = IdType.AUTO)
    private Long studentId;
    /**
     * 学号
     */
    private Long studentNum;
    /**
     * 姓名
     */
    private String studentName;
    /**
     * 密码
     */
    private String password;
    /**
     * 性别
     */
    private String sex;
    /**
     * 专业
     */
    private String domain;
    /**
     * 班级
     */
    private String grade;
    /**
     * 电话号码
     */
    private Long phone;
    /**
     * qq
     */
    private Long qq;
    /**
     * 入学年份
     */
    private Integer enrollmentYear;
    /**
     * 导师姓名
     */
    private Long teacherNum;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer ctime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer utime;
    /**
     * 逻辑删除
     */
    @TableLogic
    private Integer isDelete;
}
