package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author ：lichuanbin
 * @since : 2021/3/22 16:33
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentWeeklyNote implements Serializable {
    /**
     * 周记id
     */
    @TableId(type = IdType.AUTO)
    private Long noteId;
    /**
     * 学生号
     */
    private Long studentNum;
    /**
     * 周记正文
     */
    private String noteContent;
    /**
     * 周记评阅
     */
    private String noteEvaluate;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer ctime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer utime;
}
