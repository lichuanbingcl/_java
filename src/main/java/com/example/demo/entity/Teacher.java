package com.example.demo.entity;

import com.baomidou.mybatisplus.annotation.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author ：lichuanbin
 * @since : 2021/2/25 16:49
 *
 * 教师信息表
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("teacher")
public class Teacher implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 教师id
     */
    @TableId(type=IdType.AUTO)
    private Long teacherId;
    /**
     * 教师号
     */
    private Long teacherNum;
    /**
     * 教师姓名
     */
    private String teacherName;
    /**
     * 性别
     */
    private String sex;
    /**
     * 学科部
     */
    private String department;
    /**
     * 学位
     */
    private String degree;
    /**
     * 电话号码
     */
    private Long phone;
    /**
     * qq
     */
    private Long qq;
    /**
     * 职称
     */
    private String professional;
    /**
     * 密码
     */
    private String password;
    /**
     * 创建时间
     */
    @TableField(fill = FieldFill.INSERT)
    private Integer ctime;
    /**
     * 修改时间
     */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Integer utime;
    /**
     * 逻辑删除 1-已删除 0-未删除
     */
    @TableLogic
    private Integer isDelete;

}
