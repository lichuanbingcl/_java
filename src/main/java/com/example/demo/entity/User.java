package com.example.demo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：lichuanbin
 * @since : 2021/3/12 11:25
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class User {
    /**
     * 用户账户
     */
    private Long userAccount;
    /**
     * 密码
     */
    private String password;
    /**
     * 身份
     */
    private Integer standing;
    /**
     * 创建时间
     */
    private Integer ctime;
    /**
     * 修改时间
     */
    private Integer utime;
}
