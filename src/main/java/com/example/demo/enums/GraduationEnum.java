package com.example.demo.enums;

/**
 * @author ：lichuanbin
 * @since : 2021/3/9 11:39
 */
public enum GraduationEnum {
    /**
     * 身份级别 1-学生 2-教师
     */
    STANDING_STUDENT(true,"学生",1),
    STANDING_TUTOR(true,"教师",2);


    private Boolean option;
    private String description;
    private Integer code;

    GraduationEnum(Boolean option, String description, Integer code) {
        this.option = option;
        this.description = description;
        this.code = code;
    }

    public Boolean getOption() {
        return option;
    }

    public String getDescription() {
        return description;
    }

    public Integer getCode() {
        return code;
    }
}
