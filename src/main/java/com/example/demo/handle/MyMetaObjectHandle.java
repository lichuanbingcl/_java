package com.example.demo.handle;

import com.baomidou.mybatisplus.core.handlers.MetaObjectHandler;
import com.example.demo.utils.DateUtils;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.reflection.MetaObject;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class MyMetaObjectHandle implements MetaObjectHandler {
    /**
     * 插入时填充
     * @param metaObject
     */
    @Override
    public void insertFill(MetaObject metaObject) {
        Integer now = DateUtils.currentSecond();

        Object ctime = getFieldValByName("ctime", metaObject);
        if (null == ctime) {
            setFieldValByName("ctime", now, metaObject);
        }

        Object utime = getFieldValByName("utime", metaObject);
        if (null == utime) {
            setFieldValByName("utime", now, metaObject);
        }
    }

    /**
     * 更新时填充
     * @param metaObject
     */
    @Override
    public void updateFill(MetaObject metaObject) {
        Integer now = DateUtils.currentSecond();

        Object utime = getFieldValByName("utime", metaObject);
        if (null == utime) {
            setFieldValByName("utime", now, metaObject);
        }
    }

}
