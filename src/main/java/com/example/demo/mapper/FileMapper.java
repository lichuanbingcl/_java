package com.example.demo.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.File;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ：lichuanbin
 * @since : 2021/4/12 15:43
 */
@Mapper
public interface FileMapper extends BaseMapper<File> {
}
