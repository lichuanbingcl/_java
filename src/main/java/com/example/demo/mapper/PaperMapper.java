package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.Paper;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ：lichuanbin
 * @since : 2021/3/16 18:26
 */
@Mapper
public interface PaperMapper extends BaseMapper<Paper> {
}
