package com.example.demo.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.StudentWeeklyNote;
import org.apache.ibatis.annotations.Mapper;

/**
 * @author ：lichuanbin
 * @since : 2021/3/22 16:42
 */
@Mapper
public interface StudentWeeklyNoteMapper extends BaseMapper<StudentWeeklyNote> {
}
