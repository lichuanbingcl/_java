package com.example.demo.mapper;



import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.demo.entity.User;
import org.apache.ibatis.annotations.Mapper;


/**
 * @author ：lichuanbin
 * @since : 2021/2/25 14:56
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
}
