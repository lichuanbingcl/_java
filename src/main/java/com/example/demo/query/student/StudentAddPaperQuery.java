package com.example.demo.query.student;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/19 11:37
 */
@Data
public class StudentAddPaperQuery {
    /**
     * 论文标题
     */
    private String paperTitle;
    /**
     * 论文类型
     */
    private String paperType;
    /**
     * 学生号
     */
    private Long studentNum;
    /**
     * 论文来源
     */
    private String paperSource;
    /**
     * 论文要求
     */
    private String paperRequire;
}
