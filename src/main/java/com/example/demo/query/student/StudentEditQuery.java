package com.example.demo.query.student;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/15 16:41
 */
@Data
public class StudentEditQuery {
    private Long studentId;
    /**
     * 性别
     */
    private String sex;
    /**
     * 班级
     */
    private String grade;
    /**
     * 电话号码
     */
    private Long phone;
    /**
     * qq
     */
    private Long qq;
    /**
     * 入学年份
     */
    private Integer enrollmentYear;
}
