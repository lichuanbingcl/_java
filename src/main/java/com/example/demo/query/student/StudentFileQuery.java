package com.example.demo.query.student;

import lombok.Data;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author ：lichuanbin
 * @since : 2021/4/12 16:36
 */
@Data
public class StudentFileQuery {
    private MultipartFile file;
    private String fileDescription;
    private Long studentNum;
}
