package com.example.demo.query.student;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/22 10:49
 */
@Data
public class StudentPaperListQuery {
    private Integer currentPage;
    private Integer pageSize;
    private Long studentNum;
}
