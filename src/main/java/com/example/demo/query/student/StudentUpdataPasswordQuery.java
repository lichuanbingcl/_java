package com.example.demo.query.student;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/15 16:57
 */
@Data
public class StudentUpdataPasswordQuery {
    /**
     * 学生id
     */
    private Long studentNum;
    /**
     * 新密码
     */
    private String passwordSite;

}
