package com.example.demo.query.teacher;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/16 18:04
 */
@Data
public class PaperAddQuery {
    /**
     * 论文标题
     */
    private String paperTitle;
    /**
     * 论文类型
     */
    private String paperType;
    /**
     * 导师号
     */
    private Long teacherNum;
    /**
     * 学生号
     */
    private Long studentNum;
    /**
     * 论文来源
     */
    private String paperSource;
    /**
     * 论文要求
     */
    private String paperRequire;
}
