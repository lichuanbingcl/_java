package com.example.demo.query.teacher;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/17 15:37
 */
@Data
public class PaperEditQuery {
    /**
     * 论文id
     */
    private Long paperId;
    /**
     * 论文标题
     */
    private String paperTitle;
    /**
     * 论文类型
     */
    private String paperType;

    /**
     * 学生号
     */
    private Long studentNum;
    /**
     * 论文来源
     */
    private String paperSource;
    /**
     * 论文要求
     */
    private String paperRequire;
    /**
     * 论文状态
     */
    private String selectState;
}
