package com.example.demo.query.teacher;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/18 17:53
 */
@Data
public class PaperScoreListQuery {
    /**
     * 教师号
     */
    private Long teacherNum;
    /**
     * 当前页
     */
    private Integer currentPage;
    /**
     * 页内数量
     */
    private Integer pageSize;
}
