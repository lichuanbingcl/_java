package com.example.demo.query.teacher;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/17 16:01
 */
@Data
public class PaperSelectQuery {
    /**
     * 论文id
     */
    private Long paperId;
    /**
     * 论文标题
     */
    private String paperTitle;
    /**
     * 论文类型
     */
    private String paperType;
    /**
     * 导师号
     */
    private Long teacherNum;
    /**
     * 论文来源
     */
    private String paperSource;
    /**
     * 选中状态
     */
    private String selectState;

    private Integer currentPage;
    private Integer pageSize;
}
