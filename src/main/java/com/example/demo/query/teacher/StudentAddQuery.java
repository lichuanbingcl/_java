package com.example.demo.query.teacher;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：lichuanbin
 * @since : 2021/3/5 11:46
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class StudentAddQuery{
    /**
     * 学号
     */
    private Long studentNum;
    /**
     * 姓名
     */
    private String studentName;
    /**
     * 密码
     */
    private String password;
    /**
     * 性别
     */
    private String sex;
    /**
     * 专业
     */
    private String domain;
    /**
     * 班级
     */
    private String grade;
    /**
     * 电话号码
     */
    private Long phone;
    /**
     * qq
     */
    private Long qq;
    /**
     * 入学年份
     */
    private Integer enrollmentYear;

    /**
     * 导师名
     */
    private Long teacherNum;
}
