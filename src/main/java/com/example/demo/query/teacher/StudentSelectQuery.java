package com.example.demo.query.teacher;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/10 14:37
 *
 * 学生查询
 */
@Data
public class StudentSelectQuery {
    /**
     * 学号
     */
    private Long studentNum;
    /**
     * 姓名
     */
    private String studentName;
    /**
     * 专业
     */
    private String domain;
    /**
     * 班级
     */
    private String grade;
    /**
     * 导师姓名
     */
    private Long teacherNum;
    private Integer currentPage;
    private Integer pageSize;
}
