package com.example.demo.query.teacher;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author ：lichuanbin
 * @since : 2021/3/15 17:09
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TeacherAddQuery {
    /**
     * 教师号
     */
    private Long teacherNum;
    /**
     * 教师姓名
     */
    private String teacherName;
    /**
     * 性别
     */
    private String sex;
    /**
     * 学科部
     */
    private String department;
    /**
     * 电话号码
     */
    private Long phone;
    /**
     * 职称
     */
    private String professional;
    /**
     * 密码
     */
    private String password;
    /**
     * 学位
     */
    private String degree;
    /**
     * qq
     */
    private Long qq;
}
