package com.example.demo.query.teacher;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/15 17:17
 */
@Data
public class TeacherEditQuery {
    /**
     * 教师id
     */
    private Long teacherId;
    /**
     * 教师号
     */
    private long teacherNum;
    /**
     * 教师姓名
     */
    private String teacherName;
    /**
     * 性别
     */
    private String sex;
    /**
     * 学科部
     */
    private String department;
    /**
     * 电话号码
     */
    private Long phone;
    /**
     * 职称
     */
    private String professional;
    /**
     * 密码
     */
    private String password;
    /**
     * 学位
     */
    private String degree;
    /**
     * qq
     */
    private Long qq;
}
