package com.example.demo.query.teacher;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/10 14:33
 */
@Data
public class TeacherEditStudentQuery {
    /**
     * 学生id
     */
    private Long studentId;
    /**
     * 姓名
     */
    private String studentName;
    /**
     * 密码
     */
    private String password;
    /**
     * 性别
     */
    private String sex;
    /**
     * 专业
     */
    private String domain;
    /**
     * 班级
     */
    private String grade;
    /**
     * 电话号码
     */
    private Long phone;
    /**
     * qq
     */
    private Long qq;
    /**
     * 入学年份
     */
    private Integer enrollmentYear;
    /**
     * 导师名
     */
    private Long teacherNum;
}
