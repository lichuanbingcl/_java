package com.example.demo.query.teacher;

import lombok.Data;

@Data
public class TeacherPasswordUpdateQuery {
    private Long teacherNum;
    private String passwordSite;
}
