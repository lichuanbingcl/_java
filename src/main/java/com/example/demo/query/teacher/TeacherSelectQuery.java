package com.example.demo.query.teacher;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/15 18:06
 */
@Data
public class TeacherSelectQuery {
    /**
     * 教师号
     */
    private Long teacherNum;
    /**
     * 教师姓名
     */
    private String teacherName;
    /**
     * 学科部
     */
    private String department;
    /**
     * 学位
     */
    private String degree;
    /**
     * 职称
     */
    private String professional;

    private Integer currentPage;
    private Integer pageSize;
}
