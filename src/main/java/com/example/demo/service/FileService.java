package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.File;
import com.example.demo.query.student.StudentFileQuery;
import com.example.demo.utils.ResponseUtils;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author ：lichuanbin
 * @since : 2021/4/12 15:43
 */
public interface FileService extends IService<File> {
    /**
     * 上传文件
     * @param fileQuery
     *
     * @return
     */
    ResponseUtils upload(StudentFileQuery fileQuery) throws IOException;

    /**
     * 下载文件
     * @param response
     * @param fileId
     * @return
     */
    void download(HttpServletResponse response,String fileId) throws IOException;

    /**
     * 获取文件列表
     * @param studentNum
     * @return
     */
    ResponseUtils fileList(Long studentNum);
}
