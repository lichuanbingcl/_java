package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.dto.AuthDTO;
import com.example.demo.entity.User;
import com.example.demo.utils.ResponseUtils;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：lichuanbin
 * @since : 2021/2/25 14:54
 */
@Service
public interface IAuthService extends IService<User> {

    /**
     * 用户登录
     * @param request
     * @param response
     * @param authDTO
     * @return
     */
    ResponseUtils login(HttpServletRequest request,HttpServletResponse response,AuthDTO authDTO);

    /**
     * 用户退出
     * @param request
     * @return
     */
    ResponseUtils logout(HttpServletRequest request,HttpServletResponse response);
}
