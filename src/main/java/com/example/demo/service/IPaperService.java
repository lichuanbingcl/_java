package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.Paper;
import com.example.demo.query.teacher.PaperAddQuery;
import com.example.demo.query.teacher.PaperEditQuery;
import com.example.demo.query.teacher.PaperScoreListQuery;
import com.example.demo.query.teacher.PaperSelectQuery;
import com.example.demo.utils.ResponseUtils;
import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/16 18:23
 */
public interface IPaperService extends IService<Paper> {

    /**
     * 添加论文
     * @param addQuery
     * @return
     */
    ResponseUtils addPaper(PaperAddQuery addQuery);

    /**
     * 编辑论文
     * @param editQuery
     * @return
     */
    ResponseUtils editPaper(PaperEditQuery editQuery);

    /**
     * 查询论文列表
     * @param selectQuery
     * @return
     */
    ResponseUtils selectPaperList(PaperSelectQuery selectQuery);

    /**
     * 显示论文分数信息列表，方便添加论文分数
     * @param scoreListQuery
     * @return
     */
    ResponseUtils showPaperScoreList(PaperScoreListQuery scoreListQuery);

    /**
     * 添加分数
     * @param paperId
     * @param paperScore
     * @return
     */
    ResponseUtils addPaperScore(Long paperId,Integer paperScore);
}
