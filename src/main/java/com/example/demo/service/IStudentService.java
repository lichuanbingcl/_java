package com.example.demo.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.Student;
import com.example.demo.query.student.StudentAddPaperQuery;
import com.example.demo.query.student.StudentEditQuery;
import com.example.demo.query.student.StudentPaperListQuery;
import com.example.demo.query.student.StudentUpdataPasswordQuery;
import com.example.demo.query.teacher.StudentAddQuery;
import com.example.demo.query.teacher.TeacherEditStudentQuery;
import com.example.demo.query.teacher.StudentSelectQuery;
import com.example.demo.utils.ResponseUtils;

/**
 * @author ：lichuanbin
 * @since : 2021/2/25 16:46
 */
public interface IStudentService extends IService<Student> {
    /**
     * 添加学生
     * @param addQuery
     * @return
     */
    ResponseUtils addStudent(StudentAddQuery addQuery);

    /**
     * 删除学生
     * @param studentId
     * @return
     */
    ResponseUtils deleteStudent(Long studentId);

    /**
     * 编辑学生
     * @param editQuery
     * @return
     */
    ResponseUtils editStudent(TeacherEditStudentQuery editQuery);

    /**
     * 编辑学生
     * @param editQuery
     * @return
     */
    ResponseUtils editStudent(StudentEditQuery editQuery);

    /**
     * 编辑学生
     * @param updataPasswordy
     * @return
     */
    ResponseUtils editStudent(StudentUpdataPasswordQuery updataPasswordy);
    /**
     * 查询学生信息
     * @param selectQuery
     * @return
     */
    ResponseUtils selectStudentList(StudentSelectQuery selectQuery);

    /**
     * 论文模块
     */

    /**
     * 展示论文信息
     * @param paperListQuery
     * @return
     */
    ResponseUtils selectPaperList(StudentPaperListQuery paperListQuery);

    /**
     * 学生添加论文
     * @param addPaperQuery
     * @return
     */
    ResponseUtils addPaper(StudentAddPaperQuery addPaperQuery);

    /**
     * 学生选择论文
     * @param paperId
     * @param studentNum
     * @return
     */
    ResponseUtils studentCheckedPaper(Long paperId,Long studentNum);

    /**
     * 查询学生所选论文
     * @param studentNum
     * @return
     */
    ResponseUtils selectStudentPaper(Long studentNum);
}
