package com.example.demo.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.StudentWeeklyNote;
import com.example.demo.utils.ResponseUtils;

/**
 * @author ：lichuanbin
 * @since : 2021/3/22 16:40
 */
public interface IStudentWeeklyNoteService extends IService<StudentWeeklyNote> {
    /**
     * 学生添加周记
     * @param content
     * @param studentNum
     * @return
     */
    ResponseUtils addStudentWeeklyNote(String content,Long studentNum);

    /**
     * 显示周记列表
     * @param studentNum
     * @return
     */
    ResponseUtils showStudentWeeklyNoteList(Long studentNum);

    /**
     * 教师添加周记评价
     * @param noteId
     * @param noteEvaluate
     * @return
     */
    ResponseUtils editNote(Long noteId,String noteEvaluate);
}
