package com.example.demo.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.demo.entity.Teacher;
import com.example.demo.query.teacher.TeacherAddQuery;
import com.example.demo.query.teacher.TeacherEditQuery;
import com.example.demo.query.teacher.TeacherPasswordUpdateQuery;
import com.example.demo.query.teacher.TeacherSelectQuery;
import com.example.demo.utils.ResponseUtils;

/**
 * @author ：lichuanbin
 * @since : 2021/2/25 16:47
 */
public interface ITeacherService extends IService<Teacher> {
    /**
     * 教师编辑信息
     * @param editQuery
     * @return
     */
    ResponseUtils editTeacher(TeacherEditQuery editQuery);

    /**
     * 修改密码
     * @param updateQuery
     * @return
     */
    ResponseUtils editTeacher(TeacherPasswordUpdateQuery updateQuery);

    /**
     * 查询教师信息
     * @param selectQuery
     * @return
     */
    ResponseUtils selectTeacherList(TeacherSelectQuery selectQuery);

    /**
     * 添加教师
     * @param addQuery
     * @return
     */
    ResponseUtils addTeacher(TeacherAddQuery addQuery);
}
