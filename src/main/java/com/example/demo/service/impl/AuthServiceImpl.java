package com.example.demo.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.dto.AuthDTO;
import com.example.demo.entity.Student;
import com.example.demo.entity.User;
import com.example.demo.entity.Teacher;
import com.example.demo.enums.GraduationEnum;
import com.example.demo.mapper.StudentMapper;
import com.example.demo.mapper.TeacherMapper;
import com.example.demo.mapper.UserMapper;
import com.example.demo.service.IAuthService;
import com.example.demo.utils.CookieUtils;
import com.example.demo.utils.JwtUtils;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.TokenVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * @author ：lichuanbin
 * @since : 2021/2/25 14:54
 */
@Service
public class AuthServiceImpl extends ServiceImpl<UserMapper,User> implements IAuthService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private StudentMapper studentMapper;
    @Autowired
    private TeacherMapper tutorMapper;
    @Autowired
    private CookieUtils cookieUtils;

    @Override
    public ResponseUtils login(HttpServletRequest request, HttpServletResponse response,AuthDTO authDTO) {
        //返回值处理
        ResponseUtils responseUtils=new ResponseUtils();
        //基本判断
        if (authDTO.getUserAccount()==null||authDTO.getPassword().isEmpty()){
            return responseUtils.fail().message("成功").data("用户名或密码为空!").code(HttpStatus.OK);
        }
        //身份判断
        if (authDTO.getStanding().isEmpty()){
            return responseUtils.fail().data("请选择身份信息！").message("成功").code(HttpStatus.OK);
        }
        if(authDTO.getStanding().equals(GraduationEnum.STANDING_STUDENT.getDescription())) {
            QueryWrapper<Student> wrapper=new QueryWrapper<>();
            wrapper.eq("student_num",authDTO.getUserAccount()).eq("password",authDTO.getPassword());
            Student student=studentMapper.selectOne(wrapper);
            if (student!=null){
                String token= JwtUtils.getJwtToken(authDTO.getUserAccount(),authDTO.getStanding());
                cookieUtils.setToken(request,response,token);
                TokenVo vo=new TokenVo();
                vo.setStanding("教师");
                vo.setToken(token);
                return responseUtils.success().data(vo).message("成功").code(HttpStatus.OK);
            }
        }
        if(authDTO.getStanding().equals(GraduationEnum.STANDING_TUTOR.getDescription())) {
            QueryWrapper<Teacher> wrapper=new QueryWrapper<>();
            wrapper.eq("teacher_num",authDTO.getUserAccount()).eq("password",authDTO.getPassword());
            Teacher teacher = tutorMapper.selectOne(wrapper);
            if (teacher!=null){
                String token= JwtUtils.getJwtToken(authDTO.getUserAccount(),authDTO.getStanding());
                cookieUtils.setToken(request,response,token);
                TokenVo vo=new TokenVo();
                vo.setStanding("教师");
                vo.setToken(token);
                return responseUtils.success().data(vo).message("成功").code(HttpStatus.OK);
            }
        }
        return responseUtils.data("用户名或密码错误!").message("成功").code(HttpStatus.OK);
    }

    /**
     * 用户退出
     *
     * @param request
     * @return
     */
    @Override
    public ResponseUtils logout(HttpServletRequest request,HttpServletResponse response) {
        ResponseUtils responseUtils=new ResponseUtils();
        String token= "退出";
        cookieUtils.setToken(request,response,token);
        return responseUtils.data(true).message("成功").code(HttpStatus.OK);
    }
}
