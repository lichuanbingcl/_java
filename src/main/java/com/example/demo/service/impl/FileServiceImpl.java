package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.File;
import com.example.demo.mapper.FileMapper;
import com.example.demo.query.student.StudentFileQuery;
import com.example.demo.service.FileService;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.oms.OmsFileListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * @author ：lichuanbin
 * @since : 2021/4/12 15:44
 */
@Service
public class FileServiceImpl extends ServiceImpl<FileMapper, File> implements FileService {

    @Autowired
    private FileMapper fileMapper;
    /**
     * 上传文件
     *
     * @param fileQuery
     * @return
     */
    @Override
    public ResponseUtils upload(StudentFileQuery fileQuery) throws IOException {
        ResponseUtils responseUtils=new ResponseUtils();
        String originalFilename=fileQuery.getFile().getOriginalFilename();
        if (originalFilename.isEmpty()||originalFilename==null){
            return responseUtils.data("文件不能为空").message("成功").code(HttpStatus.OK);
        }
        BASE64Encoder encoder=new BASE64Encoder();
        String s = encoder.encode(fileQuery.getFile().getBytes());
        File file=new File();
        file.setFileContent(s);
        file.setStudentNum(fileQuery.getStudentNum());
        file.setFileDescription(fileQuery.getFileDescription());
        file.setFileName(fileQuery.getFile().getOriginalFilename());

        String fileId= UUID.randomUUID().toString();
        file.setFileId(fileId);
        return responseUtils.data(fileMapper.insert(file)==1).code(HttpStatus.OK).message("成功");

    }

    /**
     * 下载文件
     *
     * @param response
     * @param fileId
     * @return
     */
    @Override
    public void download(HttpServletResponse response, String fileId) throws IOException {
        File file=fileMapper.selectById(fileId);
        //1.设置response 响应头
        response.reset();//设置页面不缓存，清空buffer
        response.setCharacterEncoding("UTF-8");//字符编码
        response.setContentType("multipart/form-data");//二进制传输数据
        response.setHeader("Content-Disposition","attachment;fileName="+ URLEncoder.encode(file.getFileName(),"UTF-8"));


        try {
            byte[] imag= (byte[]) file.getFileContent();
            String value=new String(imag, "utf-8");
            BASE64Decoder decoder=new BASE64Decoder();
            byte[] bytes = decoder.decodeBuffer(value);
            for(int i=0;i<bytes.length;i++){
                if(bytes[i]<0){
                    bytes[i]+=256;
                }
            }
            response.setContentType("application/word");
            ServletOutputStream out = response.getOutputStream();
            out.write(bytes);
            out.flush();
            out.close();
        }catch (Exception e){
            e.printStackTrace();
        }
    }


    /**
     * 获取文件列表
     * @param studentNum
     * @return
     */
    @Override
    public ResponseUtils fileList(Long studentNum) {
        ResponseUtils responseUtils=new ResponseUtils();
        QueryWrapper<File> wrapper=new QueryWrapper<>();
        wrapper.eq("student_num",studentNum).orderByDesc("ctime");
        List<Object> files=fileMapper.selectObjs(wrapper);
        List<OmsFileListVo> vos=new ArrayList<>();
        files.forEach(file->{
            OmsFileListVo fileListVo=new OmsFileListVo();
            BeanUtils.copyProperties(file,fileListVo);
            vos.add(fileListVo);
        });
        return responseUtils.data(vos).code(HttpStatus.OK).message("成功");
    }
}
