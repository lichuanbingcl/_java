package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.Paper;
import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;
import com.example.demo.mapper.PaperMapper;
import com.example.demo.mapper.StudentMapper;
import com.example.demo.mapper.TeacherMapper;
import com.example.demo.query.teacher.PaperAddQuery;
import com.example.demo.query.teacher.PaperEditQuery;
import com.example.demo.query.teacher.PaperScoreListQuery;
import com.example.demo.query.teacher.PaperSelectQuery;
import com.example.demo.service.IPaperService;
import com.example.demo.utils.MyPage;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.oms.OmsPaperListVo;
import com.example.demo.vo.oms.OmsPaperScoreListVo;
import org.apache.commons.lang3.ObjectUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：lichuanbin
 * @since : 2021/3/16 18:25
 */
@Service
public class PaperServiceImpl extends ServiceImpl<PaperMapper, Paper> implements IPaperService {
    @Autowired
    private PaperMapper paperMapper;

    @Autowired
    private TeacherMapper teacherMapper;

    /**
     * 添加论文
     *
     * @param addQuery
     * @return
     */
    @Override
    public ResponseUtils addPaper(PaperAddQuery addQuery) {
        ResponseUtils responseUtils = new ResponseUtils();
        Map map=new HashMap();
        map.put("teacher_num",addQuery.getTeacherNum());
        if (ObjectUtils.isEmpty(teacherMapper.selectByMap(map))) {
            return responseUtils.data("请输入正确的教师号").message("成功").code(HttpStatus.OK);
        }

        Paper paper=new Paper();
        BeanUtils.copyProperties(addQuery,paper);
        paper.setSelectState("未选");
        return responseUtils.code(HttpStatus.OK).message("成功").data(paperMapper.insert(paper)==1);
    }

    /**
     * 编辑论文
     *
     * @param editQuery
     * @return
     */
    @Override
    public ResponseUtils editPaper(PaperEditQuery editQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        Paper paper=new Paper();
        BeanUtils.copyProperties(editQuery,paper);
        if(editQuery.getSelectState()=="未选"){
            paper.setStudentNum(null);
        }
        return responseUtils.code(HttpStatus.OK).message("成功").data(paperMapper.updateById(paper)==1);
    }

    /**
     * 查询论文列表
     *
     * @param selectQuery
     * @return
     */
    @Override
    public ResponseUtils selectPaperList(PaperSelectQuery selectQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        QueryWrapper<Paper> wrapper=new QueryWrapper<>();
        IPage<Paper> paperIPage=new Page<>(selectQuery.getCurrentPage(),selectQuery.getPageSize());
        MyPage<OmsPaperListVo> voMyPage=new MyPage<>();
        List<OmsPaperListVo> voList=new ArrayList<>();

        if (selectQuery.getPaperId()!=null){
            Paper paper=paperMapper.selectById(selectQuery.getPaperId());
            Map map=new HashMap();
            map.put("teacher_num",selectQuery.getTeacherNum());
            List<Teacher> teachers= teacherMapper.selectByMap(map);
            OmsPaperListVo paperListVo=new OmsPaperListVo();
            BeanUtils.copyProperties(teachers.get(0),paperListVo);
            BeanUtils.copyProperties(paper,paperListVo);
            voList.add(paperListVo);
            voMyPage.setTotal(Long.parseLong("1"));
            voMyPage.setList(voList);
            return responseUtils.data(voMyPage).message("成功").code(HttpStatus.OK);
        }

        if (!selectQuery.getPaperSource().isEmpty()){
            wrapper.like("paper_source",selectQuery.getPaperSource());
        }
        if (!selectQuery.getPaperType().isEmpty()){
            wrapper.like("paper_type",selectQuery.getPaperType());
        }
        if (!selectQuery.getPaperTitle().isEmpty()){
            wrapper.like("paper_title",selectQuery.getPaperTitle());
        }
        if (selectQuery.getTeacherNum()!=null){
            wrapper.eq("teacher_num",selectQuery.getTeacherNum());
        }
        if (!selectQuery.getSelectState().isEmpty()){
            wrapper.eq("select_state",selectQuery.getSelectState());
        }
        wrapper.orderByAsc("ctime");
        paperIPage=paperMapper.selectPage(paperIPage,wrapper);
        List<Paper> papers=paperIPage.getRecords();
        papers.forEach(paper -> {
            Map map=new HashMap();
            map.put("teacher_num",selectQuery.getTeacherNum());
            List<Teacher> teachers= teacherMapper.selectByMap(map);
            OmsPaperListVo listVo=new OmsPaperListVo();
            BeanUtils.copyProperties(paper,listVo);
            BeanUtils.copyProperties(teachers.get(0),listVo);
            voList.add(listVo);
        });
        voMyPage.setList(voList);
        voMyPage.setTotal(paperIPage.getTotal());
        return responseUtils.data(voMyPage).code(HttpStatus.OK).message("成功");
    }

    @Autowired
    private StudentMapper studentMapper;

    /**
     * 显示论文分数信息列表，方便添加论文分数
     *
     * @param scoreListQuery
     * @return
     */
    @Override
    public ResponseUtils showPaperScoreList(PaperScoreListQuery scoreListQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        if (scoreListQuery.getTeacherNum()==null){
            return responseUtils.data("请输入教师号").message("成功").code(HttpStatus.OK);
        }
        IPage<Paper> paperIPage=new Page<>(scoreListQuery.getCurrentPage(),scoreListQuery.getPageSize());
        QueryWrapper<Paper> wrapper=new QueryWrapper<>();
        MyPage<OmsPaperScoreListVo> myPage=new MyPage<>();

        wrapper.eq("teacher_num",scoreListQuery.getTeacherNum()).isNotNull("student_num");
        paperIPage=paperMapper.selectPage(paperIPage,wrapper);
        List<Paper> papers=paperIPage.getRecords();
        List<OmsPaperScoreListVo> voList=new ArrayList<>();
        papers.forEach(paper -> {
            OmsPaperScoreListVo vo=new OmsPaperScoreListVo();
            QueryWrapper<Student> studentQueryWrapper=new QueryWrapper<>();
            studentQueryWrapper.eq("student_num",paper.getStudentNum()).orderByAsc("student_num");
            Student student=studentMapper.selectOne(studentQueryWrapper);
            if (student!=null){
                BeanUtils.copyProperties(student,vo);
            }
            BeanUtils.copyProperties(paper,vo);
            voList.add(vo);
        });
        myPage.setList(voList);
        myPage.setTotal(paperIPage.getTotal());
        return responseUtils.message("成功").code(HttpStatus.OK).data(myPage);
    }

    /**
     * 添加分数
     *
     * @param paperId
     * @param paperScore
     * @return
     */
    @Override
    public ResponseUtils addPaperScore(Long paperId, Integer paperScore) {
        ResponseUtils responseUtils=new ResponseUtils();
        Paper paper=new Paper();
        paper.setPaperId(paperId);
        paper.setPaperScore(paperScore);
        return responseUtils.data(paperMapper.updateById(paper)==1).code(HttpStatus.OK).message("成功");
    }



}
