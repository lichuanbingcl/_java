package com.example.demo.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.Paper;
import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;
import com.example.demo.mapper.PaperMapper;
import com.example.demo.mapper.StudentMapper;
import com.example.demo.mapper.TeacherMapper;
import com.example.demo.query.student.StudentAddPaperQuery;
import com.example.demo.query.student.StudentEditQuery;
import com.example.demo.query.student.StudentPaperListQuery;
import com.example.demo.query.student.StudentUpdataPasswordQuery;
import com.example.demo.query.teacher.StudentAddQuery;
import com.example.demo.query.teacher.TeacherEditStudentQuery;
import com.example.demo.query.teacher.StudentSelectQuery;
import com.example.demo.service.IStudentService;
import com.example.demo.utils.MyPage;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.oms.OmsPaperListVo;
import com.example.demo.vo.oms.OmsStudentListVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：lichuanbin
 * @since : 2021/2/25 16:47
 */
@Service
@Slf4j
public class StudentServiceImpl extends ServiceImpl<StudentMapper, Student> implements IStudentService {


    @Autowired
    private StudentMapper studentMapper;

    @Autowired
    private TeacherMapper teacherMapper;

    @Autowired
    private PaperMapper paperMapper;


    /**
     * 查询学生所选论文
     *
     * @param studentNum
     * @return
     */
    @Override
    public ResponseUtils selectStudentPaper(Long studentNum) {
        ResponseUtils responseUtils=new ResponseUtils();
        Map map=new HashMap();
        map.put("student_num",studentNum);
        if (studentMapper.selectByMap(map).isEmpty()){
            return responseUtils.data("该学生不存在").code(HttpStatus.OK).message("成功");
        }
        QueryWrapper<Paper> paperQueryWrapper=new QueryWrapper<>();
        paperQueryWrapper.eq("student_num",studentNum);
        Paper paper=paperMapper.selectOne(paperQueryWrapper);
        QueryWrapper<Student> studentQueryWrapper=new QueryWrapper<>();
        studentQueryWrapper.eq("student_num",studentNum);
        Student student=studentMapper.selectOne(studentQueryWrapper);
        OmsPaperListVo vo=new OmsPaperListVo();
        if (paper==null){
            return responseUtils.data("暂未选择论文").message("成功").code(HttpStatus.OK);
        }
        QueryWrapper<Teacher> teacherQueryWrapper=new QueryWrapper<>();
        teacherQueryWrapper.eq("teacher_num",student.getTeacherNum());
        Teacher teacher=teacherMapper.selectOne(teacherQueryWrapper);
        BeanUtils.copyProperties(teacher,vo);
        BeanUtils.copyProperties(paper,vo);
        BeanUtils.copyProperties(student,vo);

        return responseUtils.code(HttpStatus.OK).message("成功").data(vo);
    }

    /**
     * 学生选择论文
     *
     * @param paperId
     * @param studentNum
     * @return
     */
    @Override
    public ResponseUtils studentCheckedPaper(Long paperId, Long studentNum) {
        ResponseUtils responseUtils=new ResponseUtils();
        Paper paper=new Paper();
        if (paperMapper.selectById(paperId).getStudentNum()!=null){
            return responseUtils.data("该论文已被选择").message("成功").code(HttpStatus.OK);
        }
        Map map=new HashMap();
        map.put("student_num",studentNum);
        if (!paperMapper.selectByMap(map).isEmpty()){
            return responseUtils.code(HttpStatus.OK).message("成功").data("你已选择论文题目");
        }
        paper.setPaperId(paperId);
        paper.setStudentNum(studentNum);
        paper.setSelectState("已选");
        return responseUtils.code(HttpStatus.OK).message("成功").data(paperMapper.updateById(paper)==1);
    }

    /**
     * 展示论文信息
     *
     * @param paperListQuery
     * @return
     */
    @Override
    public ResponseUtils selectPaperList(StudentPaperListQuery paperListQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        if (paperListQuery.getStudentNum()==null){
            return responseUtils.data("请传入学生号").code(HttpStatus.OK).message("成功");
        }
        Map map=new HashMap();
        map.put("student_num",paperListQuery.getStudentNum());
        if (studentMapper.selectByMap(map).isEmpty()){
            return responseUtils.code(HttpStatus.OK).message("成功").data("该学生不存在");
        }
        List<Student> students=studentMapper.selectByMap(map);
        IPage<Paper> paperIPage=new Page<>(paperListQuery.getCurrentPage(),paperListQuery.getPageSize());
        QueryWrapper<Paper> paperQueryWrapper=new QueryWrapper<>();
        paperQueryWrapper.eq("teacher_num",students.get(0).getTeacherNum());

        QueryWrapper<Teacher> teacherQueryWrapper=new QueryWrapper<>();
        teacherQueryWrapper.eq("teacher_num",students.get(0).getTeacherNum());
        Teacher teacher=teacherMapper.selectOne(teacherQueryWrapper);

        paperIPage=paperMapper.selectPage(paperIPage,paperQueryWrapper);

        MyPage<OmsPaperListVo> voMyPage=new MyPage<>();
        List<OmsPaperListVo> listVos=new ArrayList<>();
        paperIPage.getRecords().forEach(paper -> {
            OmsPaperListVo vo=new OmsPaperListVo();
            BeanUtils.copyProperties(teacher,vo);
            BeanUtils.copyProperties(paper,vo);
            listVos.add(vo);
        });
        voMyPage.setTotal(paperIPage.getTotal());
        voMyPage.setList(listVos);
        return responseUtils.data(voMyPage).message("成功").code(HttpStatus.OK);
    }

    /**
     * 学生添加论文
     *
     * @param addPaperQuery
     * @return
     */
    @Override
    public ResponseUtils addPaper(StudentAddPaperQuery addPaperQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        Map studentMap=new HashMap();
        studentMap.put("student_num",addPaperQuery.getStudentNum());
        List<Student> students= studentMapper.selectByMap(studentMap);
        Paper paper=new Paper();
        BeanUtils.copyProperties(addPaperQuery,paper);
        paper.setTeacherNum(students.get(0).getTeacherNum());
        paper.setSelectState("已选");
        return responseUtils.message("成功").code(HttpStatus.OK).data(paperMapper.insert(paper)==1);
    }

    /**
     * 教师查询学生信息列表
     *
     * @param selectQuery
     * @return
     */
    @Override
    public ResponseUtils selectStudentList(StudentSelectQuery selectQuery) {
        ResponseUtils responseUtils = new ResponseUtils();
        QueryWrapper<Student> studentQueryWrapper = new QueryWrapper<>();
        IPage<Student> studentIPage=new Page<>(selectQuery.getCurrentPage(),selectQuery.getPageSize());
        IPage<OmsStudentListVo> listVoIPage=new Page<>();
        if (selectQuery.getStudentNum()!=null){
            studentQueryWrapper.eq("student_num",selectQuery.getStudentNum()).orderByAsc("student_num");
            studentIPage = studentMapper.selectPage(studentIPage, studentQueryWrapper);

            List<OmsStudentListVo> listVos= new ArrayList<>();
            List<Student> students=studentIPage.getRecords();
            students.forEach(student -> {
                OmsStudentListVo vo=new OmsStudentListVo();
                QueryWrapper<Teacher> wrapper=new QueryWrapper();
                wrapper.eq("teacher_num",student.getTeacherNum());
                Teacher teacher= teacherMapper.selectOne(wrapper);
                BeanUtils.copyProperties(student,vo);
                vo.setTeacherName(teacher.getTeacherName());
                listVos.add(vo);
            });
            MyPage<OmsStudentListVo> listVoMyPage=new MyPage<>();
            listVoMyPage.setList(listVos);
            listVoMyPage.setTotal(studentIPage.getTotal());

            return responseUtils.message("成功").data(listVoMyPage).code(HttpStatus.OK);
        }

        if(!selectQuery.getDomain().isEmpty()){
            studentQueryWrapper.like("domain",selectQuery.getDomain());
        }
        if (!selectQuery.getGrade().isEmpty()){
            studentQueryWrapper.like("grade",selectQuery.getGrade());
        }
        if (!selectQuery.getStudentName().isEmpty()){
            studentQueryWrapper.like("student_name",selectQuery.getStudentName());
        }
        if (selectQuery.getTeacherNum()!=null){
            studentQueryWrapper.like("teacher_num",selectQuery.getTeacherNum());
        }
        studentQueryWrapper.orderByAsc("student_num");
        studentIPage=studentMapper.selectPage(studentIPage,studentQueryWrapper);

        List<OmsStudentListVo> listVos= new ArrayList<>();
        List<Student> students=studentIPage.getRecords();
        students.forEach(student -> {
            OmsStudentListVo vo=new OmsStudentListVo();
            QueryWrapper<Teacher> wrapper=new QueryWrapper();
            wrapper.eq("teacher_num",student.getTeacherNum());
            Teacher teacher= teacherMapper.selectOne(wrapper);
            BeanUtils.copyProperties(student,vo);
            vo.setTeacherName(teacher.getTeacherName());
            listVos.add(vo);
        });

        MyPage<OmsStudentListVo> listVoMyPage=new MyPage<>();
        listVoMyPage.setList(listVos);
        listVoMyPage.setTotal(studentIPage.getTotal());

        return responseUtils.message("成功").data(listVoMyPage).code(HttpStatus.OK);
    }

    /**
     * 教师编辑学生信息
     *
     * @param editQuery
     * @return
     */
    @Override
    public ResponseUtils editStudent(TeacherEditStudentQuery editQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        Student student= new Student();
        //JSON.parseObject(JSON.toJSONString(editQuery),Student.class);
        BeanUtils.copyProperties(editQuery,student);
        return responseUtils.data(studentMapper.updateById(student)==1).message("成功").code(HttpStatus.OK);
    }

    /**
     * 学生修改信息
     *
     * @param editQuery
     * @return
     */
    @Override
    public ResponseUtils editStudent(StudentEditQuery editQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        Student student= new Student();
        //JSON.parseObject(JSON.toJSONString(editQuery),Student.class);
        BeanUtils.copyProperties(editQuery,student);
        return responseUtils.data(studentMapper.updateById(student)==1).message("成功").code(HttpStatus.OK);
    }

    /**
     * 学生修改密码
     *
     * @param updataPassword
     * @return
     */
    @Override
    public ResponseUtils editStudent(StudentUpdataPasswordQuery updataPassword) {
        ResponseUtils responseUtils=new ResponseUtils();

        //JSON.parseObject(JSON.toJSONString(editQuery),Student.class);
        QueryWrapper<Student> wrapper=new QueryWrapper<>();
        wrapper.eq("student_num",updataPassword.getStudentNum());
        Student student=studentMapper.selectOne(wrapper);
        student.setPassword(updataPassword.getPasswordSite());
        return responseUtils.data(studentMapper.updateById(student)==1).message("成功").code(HttpStatus.OK);
    }

    /**
     * 删除学生
     *
     * @param studentId
     * @return
     */
    @Override
    public ResponseUtils deleteStudent(Long studentId) {
        ResponseUtils responseUtils=new ResponseUtils();
        if (studentMapper.selectById(studentId)==null){
            return responseUtils.data("该学生不存在").message("成功").code(HttpStatus.OK);
        }
        return responseUtils.data(studentMapper.deleteById(studentId)==1).message("成功").code(HttpStatus.OK);
    }

    /**
     * 添加学生
     *
     * @param addQuery
     * @return
     */
    @Override
    public ResponseUtils addStudent(StudentAddQuery addQuery) {
        ResponseUtils responseUtils=new ResponseUtils();

        Map map=new HashMap();
        map.put("student_num",addQuery.getStudentNum());
        if (studentMapper.selectByMap(map).size()>0){
            return responseUtils.message("成功").data("学生学号已添加").code(HttpStatus.OK);
        }

        Student student=new Student();
        //Student student=JSON.parseObject(JSON.toJSONString(addQuery),Student.class);
        BeanUtils.copyProperties(addQuery,student);
        Integer integer=studentMapper.insert(student);

        return responseUtils.data(integer==1).message("成功").code(HttpStatus.OK);
    }
}
