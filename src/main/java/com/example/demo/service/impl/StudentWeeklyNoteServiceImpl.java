package com.example.demo.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.StudentWeeklyNote;
import com.example.demo.mapper.StudentWeeklyNoteMapper;
import com.example.demo.service.IStudentWeeklyNoteService;
import com.example.demo.utils.MyPage;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.h5.H5NoteListVo;
import com.example.demo.vo.h5.H5StudentInfoVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * @author ：lichuanbin
 * @since : 2021/3/22 16:41
 */
@Service
public class StudentWeeklyNoteServiceImpl extends ServiceImpl<StudentWeeklyNoteMapper, StudentWeeklyNote> implements IStudentWeeklyNoteService {

    @Autowired
    private StudentWeeklyNoteMapper noteMapper;

    /**
     * 学生添加周记
     *
     * @param content
     * @param studentNum
     * @return
     */
    @Override
    public ResponseUtils addStudentWeeklyNote(String content, Long studentNum) {
        ResponseUtils responseUtils=new ResponseUtils();
        if (content.isEmpty()){
            return responseUtils.message("成功").code(HttpStatus.OK).data("请输入周记正文");
        }
        StudentWeeklyNote note=new StudentWeeklyNote();
        note.setStudentNum(studentNum);
        note.setNoteContent(content);
        note.setNoteEvaluate("未评阅");
        return responseUtils.data(noteMapper.insert(note)==1).code(HttpStatus.OK).message("成功");
    }

    /**
     * 显示周记列表
     *
     * @param studentNum
     * @return
     */
    @Override
    public ResponseUtils showStudentWeeklyNoteList(Long studentNum) {
        ResponseUtils responseUtils=new ResponseUtils();
        IPage<StudentWeeklyNote> iPage=new Page<>();
        QueryWrapper<StudentWeeklyNote> queryWrapper=new QueryWrapper<>();
        queryWrapper.eq("student_num",studentNum).orderByDesc("ctime");
        iPage=noteMapper.selectPage(iPage,queryWrapper);
        List<H5NoteListVo> vos=new ArrayList<>();
        iPage.getRecords().forEach(note -> {
            H5NoteListVo vo=new H5NoteListVo();
            BeanUtils.copyProperties(note,vo);
            vos.add(vo);
        });
        if (iPage.getTotal()==0){
            return responseUtils.data("该同学没有写周记").code(HttpStatus.OK).message("成功");
        }
        MyPage<H5NoteListVo> voMyPage=new MyPage<>();
        voMyPage.setTotal(iPage.getTotal());
        voMyPage.setList(vos);
        return responseUtils.message("成功").code(HttpStatus.OK).data(voMyPage);
    }

    /**
     * 教师添加周记评价
     *
     * @param noteId
     * @param noteEvaluate
     * @return
     */
    @Override
    public ResponseUtils editNote(Long noteId, String noteEvaluate) {
        ResponseUtils responseUtils=new ResponseUtils();
        if (noteEvaluate.isEmpty()){
            return responseUtils.data("请填写评价").code(HttpStatus.OK).message("成功");
        }
        StudentWeeklyNote note=new StudentWeeklyNote();
        note.setNoteEvaluate(noteEvaluate);
        note.setNoteId(noteId);
        return responseUtils.message("成功").code(HttpStatus.OK).data(noteMapper.updateById(note)==1);
    }
}
