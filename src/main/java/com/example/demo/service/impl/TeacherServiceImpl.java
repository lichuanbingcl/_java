package com.example.demo.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.demo.entity.Teacher;
import com.example.demo.mapper.TeacherMapper;
import com.example.demo.query.teacher.TeacherAddQuery;
import com.example.demo.query.teacher.TeacherEditQuery;
import com.example.demo.query.teacher.TeacherPasswordUpdateQuery;
import com.example.demo.query.teacher.TeacherSelectQuery;
import com.example.demo.service.ITeacherService;
import com.example.demo.utils.MyPage;
import com.example.demo.utils.ResponseUtils;
import com.example.demo.vo.oms.OmsTeacherListVo;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author ：lichuanbin
 * @since : 2021/2/25 16:47
 */
@Service
public class TeacherServiceImpl extends ServiceImpl<TeacherMapper, Teacher> implements ITeacherService {

    @Autowired
    private TeacherMapper teacherMapper;

    /**
     * 教师编辑信息
     *
     * @param editQuery
     * @return
     */
    @Override
    public ResponseUtils editTeacher(TeacherEditQuery editQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        Teacher teacher=new Teacher();
        BeanUtils.copyProperties(editQuery,teacher);
        return responseUtils.message("成功").data(teacherMapper.updateById(teacher)==1).code(HttpStatus.OK);
    }

    @Override
    public ResponseUtils editTeacher(TeacherPasswordUpdateQuery updateQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        QueryWrapper<Teacher> wrapper=new QueryWrapper<>();
        wrapper.eq("teacher_num",updateQuery.getTeacherNum());
        Teacher teacher=teacherMapper.selectOne(wrapper);
        teacher.setPassword(updateQuery.getPasswordSite());
        return responseUtils.message("成功").data(teacherMapper.updateById(teacher)==1).code(HttpStatus.OK);
    }

    /**
     * 查询教师信息
     *
     * @param selectQuery
     * @return
     */
    @Override
    public ResponseUtils selectTeacherList(TeacherSelectQuery selectQuery) {

        ResponseUtils responseUtils=new ResponseUtils();
        QueryWrapper<Teacher> teacherQueryWrapper = new QueryWrapper<>();
        IPage<Teacher> teacherIPage=new Page<>(selectQuery.getCurrentPage(),selectQuery.getPageSize());
        IPage<OmsTeacherListVo> listVoIPage=new Page<>();
        if (selectQuery.getTeacherNum()!=null) {
            teacherQueryWrapper.eq("teacher_num", selectQuery.getTeacherNum()).orderByAsc("teacher_num");
            teacherIPage = teacherMapper.selectPage(teacherIPage, teacherQueryWrapper);

            List<OmsTeacherListVo> listVos = new ArrayList<>();
            List<Teacher> teachers = teacherIPage.getRecords();
            teachers.forEach(teacher -> {
                OmsTeacherListVo vo = new OmsTeacherListVo();
                BeanUtils.copyProperties(teacher, vo);
                listVos.add(vo);
            });
            MyPage<OmsTeacherListVo> listVoMyPage = new MyPage<>();
            listVoMyPage.setList(listVos);
            listVoMyPage.setTotal(teacherIPage.getTotal());

            return responseUtils.message("成功").data(listVoMyPage).code(HttpStatus.OK);
        }

        if (!selectQuery.getDepartment().isEmpty()){
            teacherQueryWrapper.like("department",selectQuery.getDepartment());
        }
        if (!selectQuery.getDegree().isEmpty()){
            teacherQueryWrapper.like("degree",selectQuery.getDegree());
        }
        if (!selectQuery.getProfessional().isEmpty()){
            teacherQueryWrapper.like("professional",selectQuery.getProfessional());
        }
        if (!selectQuery.getTeacherName().isEmpty()){
            teacherQueryWrapper.like("teacher_name",selectQuery.getTeacherName());
        }
        teacherQueryWrapper.orderByAsc("teacher_num");

        teacherIPage=teacherMapper.selectPage(teacherIPage,teacherQueryWrapper);

        List<OmsTeacherListVo> listVos= new ArrayList<>();
        List<Teacher> teachers=teacherIPage.getRecords();
        teachers.forEach(teacher -> {
            OmsTeacherListVo vo=new OmsTeacherListVo();
            BeanUtils.copyProperties(teacher,vo);
            listVos.add(vo);
        });

        MyPage<OmsTeacherListVo> listVoMyPage=new MyPage<>();
        listVoMyPage.setList(listVos);
        listVoMyPage.setTotal(teacherIPage.getTotal());

        return responseUtils.message("成功").data(listVoMyPage).code(HttpStatus.OK);
    }

    /**
     * 添加教师
     *
     * @param addQuery
     * @return
     */
    @Override
    public ResponseUtils addTeacher(TeacherAddQuery addQuery) {
        ResponseUtils responseUtils=new ResponseUtils();
        if (addQuery.getTeacherNum()==null){
            return responseUtils.message("成功").data("请输入教师号").code(HttpStatus.OK);
        }
        Map map=new HashMap();
        map.put("teacher_num",addQuery.getTeacherNum());
        if (!ObjectUtils.isEmpty(teacherMapper.selectByMap(map))){
            return responseUtils.message("成功").data("教师号已存在").code(HttpStatus.OK);
        }

        Teacher teacher=new Teacher();
        BeanUtils.copyProperties(addQuery,teacher);

        return responseUtils.message("成功").data(teacherMapper.insert(teacher)==1).code(HttpStatus.OK);
    }
}
