package com.example.demo.vo;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/4/24 15:30
 */
@Data
public class TokenVo {
    private String token;
    private String standing;
}
