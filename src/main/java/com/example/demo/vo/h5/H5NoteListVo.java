package com.example.demo.vo.h5;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/22 17:55
 */
@Data
public class H5NoteListVo {
    /**
     * 周记id
     */
    private Long noteId;
    /**
     * 周记正文
     */
    private String noteContent;
    /**
     * 周记评阅
     */
    private String noteEvaluate;
    /**
     * 创建时间
     */
    private Integer ctime;
}
