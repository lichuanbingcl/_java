package com.example.demo.vo.h5;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/10 15:26
 */
@Data
public class H5StudentInfoVo {
    /**
     * 学生id
     */
    private Long studentId;
    /**
     * 学号
     */
    private Long studentNum;
    /**
     * 姓名
     */
    private String studentName;
    /**
     * 性别
     */
    private String sex;
    /**
     * 专业
     */
    private String domain;
    /**
     * 班级
     */
    private String grade;
    /**
     * 电话号码
     */
    private Long phone;
    /**
     * qq
     */
    private Long qq;
    /**
     * 入学年份
     */
    private Integer enrollmentYear;
    /**
     * 教师工号
     */
    private String teacherName;
}
