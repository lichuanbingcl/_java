package com.example.demo.vo.oms;

import lombok.Data;

@Data
public class OmsFileListVo {
    private Integer ctime;
    private String fileId;
    private String fileName;
    private String fileDescription;
}
