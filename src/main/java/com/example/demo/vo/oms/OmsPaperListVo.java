package com.example.demo.vo.oms;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/17 16:23
 */
@Data
public class OmsPaperListVo {
    /**
     * 学生号
     */
    private Long studentNum;
    /**
     * 论文id
     */
    private Long paperId;
    /**
     * 论文标题
     */
    private String paperTitle;
    /**
     * 论文类型
     */
    private String paperType;
    /**
     * 论文来源
     */
    private String paperSource;
    /**
     * 论文要求
     */
    private String paperRequire;
    /**
     * 导师姓名
     */
    private String teacherName;
    /**
     * 导师号
     */
    private String teacherNum;
    /**
     * 性别
     */
    private String sex;
    /**
     * 职称
     */
    private String professional;
    /**
     * 学位
     */
    private String degree;
    /**
     * 电话号码
     */
    private Long phone;
    /**
     * qq
     */
    private Long qq;
    /**
     * 学科部
     */
    private String department;
    /**
     * 选中状态 已选 未选
     */
    private String selectState;
}
