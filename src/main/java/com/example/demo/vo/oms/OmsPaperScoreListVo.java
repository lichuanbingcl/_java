package com.example.demo.vo.oms;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/18 17:47
 */
@Data
public class OmsPaperScoreListVo {
    /**
     * 论文id
     */
    private Long paperId;
    /**
     * 论文标题
     */
    private String paperTitle;
    /**
     * 学生号
     */
    private Long studentNum;
    /**
     * 学生姓名
     */
    private String studentName;
    /**
     * 论文分数
     */
    private Integer paperScore;
}
