package com.example.demo.vo.oms;

import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/16 10:47
 */
@Data
public class OmsStudentListVo {
    /**
     * 学生id
     */
    private Long studentId;
    /**
     * 学号
     */
    private Long studentNum;
    /**
     * 姓名
     */
    private String studentName;
    /**
     * 密码
     */
    private String password;
    /**
     * 性别
     */
    private String sex;
    /**
     * 专业
     */
    private String domain;
    /**
     * 班级
     */
    private String grade;
    /**
     * 电话号码
     */
    private Long phone;
    /**
     * qq
     */
    private Long qq;
    /**
     * 入学年份
     */
    private Integer enrollmentYear;
    /**
     * 教师名
     */
    private String teacherName;
    /**
     * 教师号
     */
    private Long teacherNum;
}
