package com.example.demo.vo.oms;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

/**
 * @author ：lichuanbin
 * @since : 2021/3/15 18:21
 */
@Data
public class OmsTeacherListVo {
    /**
     * 教师id
     */
    private Long teacherId;
    /**
     * 教师号
     */
    private Long teacherNum;
    /**
     * 教师姓名
     */
    private String teacherName;
    /**
     * 性别
     */
    private String sex;
    /**
     * 学科部
     */
    private String department;
    /**
     * 学位
     */
    private String degree;
    /**
     * 电话号码
     */
    private Long phone;
    /**
     * qq
     */
    private Long qq;
    /**
     * 职称
     */
    private String professional;
    /**
     * 密码
     */
    private String password;
}
